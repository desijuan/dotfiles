#!/bin/bash

# convert DjVu to PDF
# usage:  djvu2pdf.sh  <file.djvu>

i="$1"
o="`basename $i .djvu`"
o="$o".pdf

cmd="ddjvu -format=pdf $i $o "
#cmd="ddjvu -format=pdf -mode=black $i $o "

$cmd

