#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

#alias chau='shutdown now'
alias chau='systemctl poweroff'
alias ribut='systemctl reboot'
alias zzz='systemctl hibernate'
alias int='ping www.google.com'
alias gis='git status'
alias bat='acpi -i'
alias supac='sudo pacman'
alias rg='ranger --choosedir=$HOME/.config/ranger/rangerdir ; cd "$(cat $HOME/.config/ranger/rangerdir)"'
alias grep='grep --color=always'
alias sclip="xclip -sel c"
alias ace='acestream-launcher'
alias bc='bc -l'
alias l='ledger b'
alias ca='cal -3'

# Para que anden bien los colores en Xterm, pero me caga los colores en la terminal común =(.
export TERM='xterm-256color'

export VISUAL=vim
export EDITOR="$VISUAL"

source ~/.git-completion.bash

PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

